/// @file recap14.cpp

#if defined(__cplusplus) && __cplusplus < 201402L
#error "This file requires c++ standard 14 or higher"
#endif

#include "StandardLibrary.hpp"

// ========================================================================
// Helper Functions
// ========================================================================

void PrintMessage(const char *message) { std::cout << message << std::endl; }

template <typename T>
void PrintData(T &&data, const int size, const char *name) {
	std::cout << name << ": [ ";
	for (int i = 0; i < size; i++) {
		std::cout << data[i] << ' ';
	}
	std::cout << "]" << std::endl;
}

// ========================================================================
// Helper Classes
// ========================================================================

class Noisy {
  public:
	// Rule of Five
	Noisy() { puts("Noisy:: default ctor"); }
	Noisy(int newValue) : mValue(newValue) { puts("Noisy:: implicit ctor"); }
	Noisy(const Noisy &other) {
		puts("Noisy:: copy ctor");
		mValue = other.mValue;
	};
	Noisy(Noisy &&other) noexcept {
		puts("Noisy:: move ctor");
		mValue = other.mValue;
		other.mValue = 0;
	}
	Noisy &operator=(const Noisy &other) {
		puts("Noisy:: copy assign");
		if (this != &other) {
			mValue = other.mValue;
		}
		return *this;
	}
	Noisy &operator=(Noisy &&other) noexcept {
		puts("Noisy:: move assign");
		if (this != &other) {
			mValue = other.mValue;
			other.mValue = 0;
		}
		return *this;
	}
	~Noisy() { puts("Noisy :: dtor"); }

	// int operator (converts this class into an int)
	operator int() { return mValue; }

	/// @note friend function to allow cout to print
	friend std::ostream &operator<<(std::ostream &os, const Noisy &rhs);

  private:
	int mValue;
};

std::ostream &operator<<(std::ostream &os, const Noisy &rhs) {
	os << rhs.mValue << ' ';
	return os;
}

// the `main` function is the entry point of the application
int main() {

	// ========================================================================
	// Standard Template Library
	// ========================================================================

	// ------------------------------------------------------------------------
	// Vectors - dynamically allocated array
	// ------------------------------------------------------------------------

	{
		// creates a vector of 5 Noisy elements
		std::vector<Noisy> numbers;
		for (int i = 0; i < 5; i++) {
			numbers.push_back(i);
		}
	}
	puts("//////////////////////////////////////////////////////////////");
	{
		// creates a vector of 5 Noisy elements
		std::vector<Noisy> numbers;
		numbers.reserve(5);
		for (int i = 0; i < 5; i++) {
			numbers.push_back(i);
		}
	}
	puts("//////////////////////////////////////////////////////////////");
	{
		// creates a vector of 5 Noisy elements
		std::vector<Noisy> numbers;
		// ! don't forget to reserve capacity for the elements
		numbers.reserve(5);
		for (int i = 0; i < 5; i++) {
			numbers.emplace_back(i);
		}
		// new    > ptr = malloc
		// delete > free(ptr)
		PrintData(numbers.data(), numbers.size(), "numbers");
	}

	// ------------------------------------------------------------------------
	// Arrays - dynamically allocated array
	// ------------------------------------------------------------------------

	{
		puts("Array ========================================================");
		std::array<Noisy, 5> numbers{1, 2, 3, 4, 5};
		PrintData(numbers.data(), numbers.size(), "numbers Array");
		std::cout << "size: " << numbers.size() << std::endl;
	}

	// //
	// ------------------------------------------------------------------------
	// // Stack
	// //
	// ------------------------------------------------------------------------

	{
		puts("Stack ========================================================");
		puts("lhs");
		std::stack<Noisy> lhs;
		for (int i = 0; i < 5; i++) {
			lhs.push(i);
		}

		puts("rhs");
		std::stack<Noisy> rhs;
		for (int i = 0; i < 5; i++) {
			rhs.emplace(i);
		}

		puts("swap");
		lhs.swap(rhs);
		puts("after swap");
	}

	// //
	// ------------------------------------------------------------------------
	// // Queue
	// //
	// ------------------------------------------------------------------------

	{
		puts("Queue ========================================================");
		puts("lhs");
		std::queue<Noisy> lhs;
		for (int i = 0; i < 5; i++) {
			lhs.push(i);
		}

		puts("rhs");
		std::queue<Noisy> rhs;
		for (int i = 0; i < 5; i++) {
			rhs.emplace(i);
		}

		puts("swap");
		lhs.swap(rhs);
		puts("after swap");

		for (int i = 0; i < 5; i++) {
			lhs.pop();
		}
	}

	// //
	// ------------------------------------------------------------------------
	// // Deque
	// //
	// ------------------------------------------------------------------------

	{
		puts("Deque ========================================================");

		std::deque<Noisy> numbers;
		for (int i = 0; i < 5; i++) {
			numbers.emplace_back(i + 1);
		}

		const auto PrintDeque = [](const std::deque<Noisy> &d) {
			std::cout << "numbers Deque: [ ";
			for (const auto &n : d) {
				std::cout << n << ' ';
			}
			std::cout << "]\n";
		};

		PrintDeque(numbers);
		numbers.emplace_back(6);
		PrintDeque(numbers);
		numbers.emplace_front(0);
		PrintDeque(numbers);
		numbers.pop_back();
		numbers.pop_front();
		PrintDeque(numbers);
	}

	// //
	// ------------------------------------------------------------------------
	// // List
	// //
	// ------------------------------------------------------------------------

	{
		puts("List ========================================================");

		std::list<Noisy> numbers;
		for (int i = 0; i < 5; i++) {
			numbers.emplace_back(i + 1);
		}

		std::cout << "list numbers: [ ";
		for (const auto &noisy : numbers) {
			std::cout << noisy << ' ';
		}
		std::cout << "]\n";

		numbers.reverse();

		std::cout << "list numbers: [ ";
		for (const auto &noisy : numbers) {
			std::cout << noisy << ' ';
		}
		std::cout << "]\n";
	}

	return 0;
}
