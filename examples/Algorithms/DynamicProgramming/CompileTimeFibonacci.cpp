#include "../../../include/StandardLibrary.hpp"

// clang-format off
template<uint64_t I>
struct CompileFib{
    static const std::size_t value = 
        CompileFib<(I-1)>::value +
        CompileFib<(I-2)>::value;
};

// base case
template <>
struct CompileFib<0> {
    static const std::size_t value = 0;
};

template <>
struct CompileFib<1> {
    static const std::size_t value = 1;
};

// clang-format on

int main() {
	constexpr auto fib6 = CompileFib<6>::value;
	static_assert(fib6 == 8, "Failed to compile Fibonacci(6)");
	return 0;
}
