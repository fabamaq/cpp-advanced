#include <cstdio>

#include <chrono>
#include <optional>
#include <string>
#include <unordered_map>

uint64_t Fibonacci(uint64_t n) {
	// base case
	if (n < 2) {
		return n;
	}
	// recursive case
	return Fibonacci(n - 1) + Fibonacci(n - 2);
}

void Test_Fibonacci(uint64_t n) {
	auto start = std::chrono::high_resolution_clock::now();
	for (uint64_t i = 0; i < n; i++) {
		printf("Fibonacci(%lu) = %lu \n", i, Fibonacci(i));
	}
	auto end = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> duration = end - start;
	printf("Took: %f seconds\n", duration.count());
}

uint64_t MemoizedFibonacciAux(uint64_t n,
							  std::unordered_map<uint64_t, uint64_t> &cache) {
	// if cache.contains(n) << c++20
	if (cache.count(n) > 0) {
		return cache.at(n);
	}

	// base case
	if (n < 2) {
		return n;
	}

	auto result =
		MemoizedFibonacciAux(n - 1, cache) + MemoizedFibonacciAux(n - 2, cache);

	cache[n] = result;
	return result;
}

uint64_t MemoizedFibonacci(uint64_t n) {
	std::unordered_map<uint64_t, uint64_t> cache{};
	return MemoizedFibonacciAux(n, cache);
}

void Test_MemoizedFibonacci(uint64_t n) {
	auto start = std::chrono::high_resolution_clock::now();
	for (uint64_t i = 0; i < n; i++) {
		printf("Fibonacci(%lu) = %lu \n", i, MemoizedFibonacci(i));
	}
	auto end = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> duration = end - start;
	printf("Took: %f seconds\n", duration.count());
}

/******************************************************************************
 * @brief Main - Application entry point
 *****************************************************************************/

std::optional<uint64_t> AsInt(const std::string &s) {
	try {
		return std::stoi(s);
	} catch (...) {
		return std::nullopt;
	}
}

int main(int argc, char **argv) {
	uint64_t n = 42;
	if (argc > 1) {
		auto tmp = AsInt(argv[1]);
		if (tmp.has_value()) {
			n = tmp.value();
		}
	}

	Test_Fibonacci(n);
	puts("//////////////////////////////////////////////");
	Test_MemoizedFibonacci(n);
	return 0;
}
