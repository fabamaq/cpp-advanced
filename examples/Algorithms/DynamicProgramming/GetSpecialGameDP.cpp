#include "../../../include/StandardLibrary.hpp"

// ----------------------------------------------------------------------------
// Returns that it is possible to sum values up to the target value
// ----------------------------------------------------------------------------
const std::vector<int> &getBonusValues() {
	static const std::vector<int> bonusValues{100, 150, 200, 250, 300};
	return bonusValues;
}

// ----------------------------------------------------------------------------
// Returns that it is possible to sum values up to the target value
// ----------------------------------------------------------------------------
const std::vector<int> &getBonusPrizeTable() {
	static const std::vector<int> bonusPrizeTable{10, 50, 100, 300};
	return bonusPrizeTable;
}

// ----------------------------------------------------------------------------
// Returns if it is possible to sum values up to the target value
// ----------------------------------------------------------------------------
bool canSum(int targetValue, const std::vector<int> &values,
			std::unordered_map<int, bool> &cache) {
	if (cache.count(targetValue) > 0) {
		return cache[targetValue];
	}
	// base case
	if (targetValue == 0) {
		return true;
	}
	if (targetValue < 0) {
		return false;
	}

	// recursive case
	for (const auto &pieceValue : values) {
		if (canSum((targetValue - pieceValue), values, cache) == true) {
			cache[targetValue] = true;
			return true;
		}
	}

	cache[targetValue] = false;
	return false;
}

// ----------------------------------------------------------------------------
// Returns if it is possible to sum values up to the target value
// ----------------------------------------------------------------------------
bool canSum(int targetValue, const std::vector<int> &values) {
	std::unordered_map<int, bool> cache{};
	return canSum(targetValue, values, cache);
}

// ----------------------------------------------------------------------------
// Returns one possible combination of values that sum-up to the target value
// ----------------------------------------------------------------------------
std::vector<int> howSum(int targetValue, const std::vector<int> &values,
						std::unordered_map<int, std::vector<int>> &cache) {
	if (cache.count(targetValue) > 0) {
		return cache[targetValue];
	}
	if (targetValue == 0) {
		return {};
	}
	if (targetValue < 0) {
		return {{-1}};
	}

	const auto isNull = [](const std::vector<int> &data) {
		if (data.empty()) {
			return false;
		}
		return data.front() == -1;
	};

	const auto spread = [](std::vector<int> data, int newValue) {
		data.push_back(newValue);
		return data;
	};

	for (const auto &pieceValue : values) {
		const auto remainder = targetValue - pieceValue;
		const auto remainderResult = howSum(remainder, values, cache);
		if (!isNull(remainderResult)) {
			cache[targetValue] = spread(remainderResult, pieceValue);
			return cache[targetValue];
		}
	}

	cache[targetValue] = {-1};
	return cache[targetValue];
}

// ----------------------------------------------------------------------------
// Returns one possible combination of values that sum-up to the target value
// ----------------------------------------------------------------------------
std::vector<int> howSum(int targetValue, const std::vector<int> &values) {
	std::unordered_map<int, std::vector<int>> cache{};
	return howSum(targetValue, values, cache);
}

// ----------------------------------------------------------------------------
// Returns all combinations of values that sum-up to the target value
// ----------------------------------------------------------------------------
std::vector<std::vector<int>>
allSum(int targetValue, const std::vector<int> &values,
	   std::unordered_map<int, std::vector<std::vector<int>>> &cache) {
	if (cache.count(targetValue) > 0) {
		return cache[targetValue];
	}
	if (targetValue == 0) {
		return {{}};
	}
	if (targetValue < 0) {
		return {};
	}

	const auto spread = [](std::vector<int> data, int newValue) {
		data.push_back(newValue);
		return data;
	};

	using combination_t = std::vector<int>;
	std::vector<combination_t> result{};

	for (const auto &value : values) {
		const auto suffix = targetValue - value;
		auto suffixWays = allSum(suffix, values, cache);
		auto targetWays = suffixWays;
		std::for_each(
			targetWays.begin(), targetWays.end(),
			[value](std::vector<int> &way) { way.insert(way.begin(), value); });

		for (auto &&way : targetWays) {
			result.push_back(way);
		}
	}

	cache[targetValue] = result;
	return result;
}

// ----------------------------------------------------------------------------
// Returns all combinations of values that sum-up to the target value
// ----------------------------------------------------------------------------
std::vector<std::vector<int>> allSum(int targetValue,
									 const std::vector<int> &values) {
	std::unordered_map<int, std::vector<std::vector<int>>> cache{};
	return allSum(targetValue, values, cache);
}

// ----------------------------------------------------------------------------
// Returns the best combination of values that sum-up to the target value
// ----------------------------------------------------------------------------
std::vector<int>
bestSum(int targetValue, const std::vector<int> &values,
		const std::function<bool(const std::vector<int> &lhs,
								 const std::vector<int> &rhs)> &fn,
		std::unordered_map<int, std::vector<int>> &cache) {

	if (cache.count(targetValue) > 0) {
		return cache[targetValue];
	}

	const auto isNull = [](const std::vector<int> &data) {
		if (data.empty()) {
			return false;
		}
		return data.front() == -1;
	};

	const auto spread = [](std::vector<int> data, int newValue) {
		data.push_back(newValue);
		return data;
	};

	// base case
	if (targetValue == 0) {
		return {};
	}
	if (targetValue < 0) {
		return {-1};
	}

	std::vector<int> bestCombination{-1};

	for (const auto &value : values) {
		const auto remainder = targetValue - value;
		const auto remainderCombination = bestSum(remainder, values, fn, cache);
		if (!isNull(remainderCombination)) {
			auto combination = spread(remainderCombination, value);
			if (isNull(bestCombination) || fn(combination, bestCombination)) {
				bestCombination = combination;
			}
		}
	}

	cache[targetValue] = bestCombination;
	return bestCombination;
}

// ----------------------------------------------------------------------------
// Returns the best combination of values that sum-up to the target value
// ----------------------------------------------------------------------------
std::vector<int>
bestSum(int targetValue, const std::vector<int> &values,
		const std::function<bool(const std::vector<int> &lhs,
								 const std::vector<int> &rhs)> &fn) {
	std::unordered_map<int, std::vector<int>> cache{};
	return bestSum(targetValue, values, fn, cache);
}

// ----------------------------------------------------------------------------
// Prints a vector (duh)
// ----------------------------------------------------------------------------
void PrintVector(const std::vector<int> &data) {
	printf("data: [ ");
	for (const auto &value : data) {
		printf("%d ", value);
	}
	puts("]");
}

void PrintVectors(const std::vector<std::vector<int>> &vectors) {
	if (vectors.empty()) {
		puts("vectors: []");
		return;
	}
	printf("vectors:\n");
	for (const auto &data : vectors) {
		printf("[ ");
		for (const auto &value : data) {
			printf("%d ", value);
		}
		puts("]");
	}
}

/******************************************************************************
 * @brief Application entry point
 *****************************************************************************/
int main() {
	//   puts("canSum(7, {2, 3})");
	//   printf("%s\n", (canSum(7, {2, 3}) ? "true" : "false"));
	//   puts("canSum(7, {5, 3, 4, 7})");
	//   printf("%s\n", (canSum(7, {5, 3, 4, 7}) ? "true" : "false"));
	//   puts("canSum(7, {2, 4})");
	//   printf("%s\n", (canSum(7, {2, 4}) ? "true" : "false"));
	//   puts("canSum(8, {2, 3, 5})");
	//   printf("%s\n", (canSum(8, {2, 3, 5}) ? "true" : "false"));
	//   puts("canSum(300, {7, 14})");
	//   printf("%s\n", (canSum(300, {7, 14}) ? "true" : "false"));

	//   puts("howSum(7, {2, 3})");
	//   PrintVector(howSum(7, {2, 3}));
	//   puts("howSum(7, {5, 3, 4, 7})");
	//   PrintVector(howSum(7, {5, 3, 4, 7}));
	//   puts("howSum(7, {2, 4})");
	//   PrintVector(howSum(7, {2, 4}));
	//   puts("howSum(8, {2, 3, 5})");
	//   PrintVector(howSum(8, {2, 3, 5}));
	//   puts("howSum(300, {7, 14})");
	//   PrintVector(howSum(300, {7, 14}));

	//   puts("countSum(7, {2, 3, 4, 7})");
	//   auto r1 = allSum(7, {2, 3, 4, 7});
	//   printf("%lu\n", r1.size());
	//   puts("countSum(8, {2, 3, 4, 5})");
	//   auto r2 = allSum(8, {2, 3, 4, 5});
	//   printf("%lu\n", r2.size());
	//   puts("countSum(300, {7, 14})");
	//   auto r3 = allSum(300, {7, 14});
	//   printf("%lu\n", r3.size());

	//   puts("allSum(7, {2, 3, 4, 7})");
	//   PrintVectors(r1);
	//   puts("allSum(8, {2, 3, 4, 5})");
	//   PrintVectors(r2);
	//   puts("allSum(300, {7, 14})");
	//   PrintVectors(r3);

	//   const auto bestCriteria = [](const std::vector<int> &lhs,
	//                                const std::vector<int> &rhs) {
	//     return lhs.size() < rhs.size();
	//   };
	//   puts("bestSum(7, {5, 3, 4, 7}, ...)");
	//   PrintVector(bestSum(7, {5, 3, 4, 7}, bestCriteria));
	//   puts("bestSum(8, {2, 3, 5}, ...)");
	//   PrintVector(bestSum(8, {2, 3, 5}, bestCriteria));
	//   puts("bestSum(8, {1, 4, 5}, ...)");
	//   PrintVector(bestSum(8, {1, 4, 5}, bestCriteria));
	//   puts("bestSum(100, {1, 2, 5, 25}, ...)");
	//   PrintVector(bestSum(100, {1, 2, 5, 25}, bestCriteria));

	const auto &bonusValues = getBonusValues();
	const auto &bonusPrizeTable = getBonusPrizeTable();

	std::unordered_map<int, bool> canSumCache{};
	if (std::all_of(bonusValues.cbegin(), bonusValues.cend(),
					[&canSumCache, &bonusPrizeTable](const int &value) {
						return canSum(value, bonusPrizeTable, canSumCache);
					})) {
		puts("Can construct all of them");
	} else {
		puts("Cannot construct all of them");
	}

	std::unordered_map<int, std::vector<int>> howSumCache{};
	std::for_each(bonusValues.cbegin(), bonusValues.cend(),
				  [&howSumCache, &bonusPrizeTable](const int &value) {
					  printf("howSum(%d, ...)\n", value);
					  PrintVector(howSum(value, bonusPrizeTable, howSumCache));
				  });

	std::unordered_map<int, std::vector<std::vector<int>>> allSumCache{};
	std::for_each(
		bonusValues.cbegin(), bonusValues.cend(),
		[&allSumCache, &bonusPrizeTable](const int &value) {
			printf("allSum(%d, ...)\n", value);
			auto allSumResult = allSum(value, bonusPrizeTable, allSumCache);
			decltype(allSumResult) allSumFiltered;
			allSumFiltered.reserve(allSumResult.size());
			std::copy_if(allSumResult.cbegin(), allSumResult.cend(),
						 std::back_inserter(allSumFiltered),
						 [](const std::vector<int> &data) {
							 return data.size() ==
									18; // number of pieces to level up
						 });
			PrintVectors(allSumFiltered);
		});

	const auto bonusRules = [](const std::vector<int> &lhs,
							   const std::vector<int> &rhs) {
		(void)lhs;
		return rhs.size() > 2;
	};

	std::unordered_map<int, std::vector<int>> bestSumCache{};
	std::for_each(
		bonusValues.cbegin(), bonusValues.cend(),
		[&bestSumCache, &bonusPrizeTable, &bonusRules](const int &value) {
			PrintVector(bestSum(value, bonusPrizeTable, bonusRules));
		});

	return 0;
}
