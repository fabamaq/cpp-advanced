#include <cstdio>

#include <algorithm>
#include <chrono>
#include <limits>
#include <random>
#include <vector>

/******************************************************************************
 * Constants
 *****************************************************************************/

constexpr auto cxStressTestSize = 32;

/******************************************************************************
 * Helpers Functions
 *****************************************************************************/

void PrintData(int *data, int size, const char *name) {
	printf("%s: [ ", name);
	for (int i = 0; i < size; i++) {
		printf("%d ", data[i]);
	}
	puts("]");
}

int Random(int a, int b) {
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<int> distrib(a, b);
	return distrib(gen);
}

std::vector<int> GenerateRandomData() {
	static std::vector<int> prices;
	if (prices.empty()) {
		prices.reserve(cxStressTestSize);
		while (prices.size() < cxStressTestSize) {
			prices.push_back(Random(1, cxStressTestSize));
		}
	}
	return prices;
}

/******************************************************************************
 * Rod cutting problem
 *
 * A rod of length N inches and a table of prices Pi for i = 1,2,...N,
 * determine the maximum revenue Rn obtainable by cutting up the rod and selling
 * the pices.
 *****************************************************************************/

int CutRod(int *p, int n) {
	if (n == 0) {
		return 0;
	}
	int q = std::numeric_limits<int>::min();
	for (int i = 0; i < n; i++) {
		q = std::max(q, p[i] + CutRod(p, n - (i + 1)));
	}
	return q;
}

void Test_CutRod_Algorithm() {
	int prices[] = {1, 5, 8, 9, 10, 17, 17, 20, 24, 30};
	constexpr int size = sizeof(prices) / sizeof(int);
	for (int i = 1; i <= size; i++) {
		auto result = CutRod(prices, i);
		printf("result[%02d]: %d\n", i, result);
	}
}

void StressTest_CutRod_Algorithm() {
	auto prices = GenerateRandomData();
	PrintData(prices.data(), prices.size(), "CutRod prices");
	auto start = std::chrono::high_resolution_clock::now();
	auto longResult = CutRod(prices.data(), cxStressTestSize - 1);
	auto end = std::chrono::high_resolution_clock::now();
	printf("longResult: %d\n", longResult);
	std::chrono::duration<double> duration = end - start;
	printf("Took: %f seconds\n", duration.count());
}

int MemoizedCutRodAux(int *p, int n, int *r) {
	if (r[n - 1] >= 0) {
		return r[n - 1];
	}
	int q;
	if (n == 0) {
		q = 0;
	} else {
		q = std::numeric_limits<int>::min();
		for (int i = 0; i < n; i++) {
			q = std::max(q, p[i] + MemoizedCutRodAux(p, n - (i + 1), r));
		}
	}
	r[n - 1] = q;
	return q;
}

int MemoizedCutRod(int *p, int n) {
	std::vector<int> r(static_cast<std::size_t>(n));
	const int min = std::numeric_limits<int>::min();
	for (int i = 0; i < n; i++) {
		r[static_cast<std::size_t>(i)] = min;
	}
	return MemoizedCutRodAux(p, n, r.data());
}

void Test_MemoizedCutRod_Algorithm() {
	int prices[] = {1, 5, 8, 9, 10, 17, 17, 20, 24, 30};
	constexpr int size = sizeof(prices) / sizeof(int);
	for (int i = 1; i <= size; i++) {
		auto result = MemoizedCutRod(prices, i);
		printf("result[%02d]: %d\n", i, result);
	}
}

void StressTest_MemoizedCutRod_Algorithm() {
	auto prices = GenerateRandomData();
	PrintData(prices.data(), prices.size(), "MemoizedCutRod prices");
	auto start = std::chrono::high_resolution_clock::now();
	auto longResult = MemoizedCutRod(prices.data(), cxStressTestSize - 1);
	auto end = std::chrono::high_resolution_clock::now();
	printf("longResult: %d\n", longResult);
	std::chrono::duration<double> duration = end - start;
	printf("Took: %f seconds\n", duration.count());
}

int BottomUpCutRod(int *p, int n) {
	const auto uN = static_cast<std::size_t>(n);
	//  let r[0..n] be a new array
	std::vector<int> r(uN + 1);

	r[0] = 0;

	// for j = 1 to n
	for (auto j = 1U; j <= uN; j++) {
		// q = -infinity
		auto q = std::numeric_limits<int>::min();
		// for i = 1 to j
		for (auto i = 1U; i <= j; i++) {
			q = std::max(q, p[i] + r[j - i]);
		}
		r[j] = q;
	}
	return r[uN];
}

void Test_BottomUpCutRod_Algorithm() {
	int prices[] = {0, 1, 5, 8, 9, 10, 17, 17, 20, 24, 30};
	constexpr int size = sizeof(prices) / sizeof(int);
	for (int i = 1; i < size; i++) {
		auto result = BottomUpCutRod(prices, i);
		printf("result[%02d]: %d\n", i, result);
	}
}

void StressTest_BottomUpCutRod_Algorithm() {
	auto prices = GenerateRandomData();
	prices.insert(prices.begin(), 0);
	PrintData(prices.data(), prices.size(), "BottomUpCutRod prices");
	auto start = std::chrono::high_resolution_clock::now();
	auto longResult = BottomUpCutRod(prices.data(), cxStressTestSize - 1);
	auto end = std::chrono::high_resolution_clock::now();
	printf("longResult: %d\n", longResult);
	std::chrono::duration<double> duration = end - start;
	printf("Took: %f seconds\n", duration.count());
}

std::pair<std::vector<int>, std::vector<int>> ExtendedBottomUpCutRod(int *p,
																	 int n) {
	const auto uN = static_cast<std::size_t>(n);
	//  let r[0..n] be a new array
	std::vector<int> r(uN + 1);
	std::vector<int> s(uN + 1);

	r[0] = 0;

	// for j = 1 to n
	for (auto j = 1U; j <= uN; j++) {
		// q = -infinity
		auto q = std::numeric_limits<int>::min();
		// for i = 1 to j
		for (auto i = 1U; i <= j; i++) {
			if (q < (p[i] + r[j - i])) {
				q = p[i] + r[j - i];
				s[j] = static_cast<int>(i);
			}
		}
		r[j] = q;
	}

	return {r, s};
}

void PrintCutRodSolution(int *p, int n) {
	auto [r, s] = ExtendedBottomUpCutRod(p, n);
	while (n > 0) {
		printf("%d ", s[static_cast<std::size_t>(n)]);
		n -= s[static_cast<std::size_t>(n)];
	}
}


void Test_PrintCutRodSolution() {
	int prices[] = {0, 1, 5, 8, 9, 10, 17, 17, 20, 24, 30};
	constexpr int size = sizeof(prices) / sizeof(int);
	for (int i = 1; i < size; i++) {
		printf("Cuts for length %d : [ ", i);
		PrintCutRodSolution(prices, i);
		puts("]");
	}
}

/******************************************************************************
 * @brief Main - Application entry point
 *****************************************************************************/
int main(int argc, char **argv) {
	Test_CutRod_Algorithm();
	StressTest_CutRod_Algorithm();

	Test_MemoizedCutRod_Algorithm();
	StressTest_MemoizedCutRod_Algorithm();

	Test_BottomUpCutRod_Algorithm();
	StressTest_BottomUpCutRod_Algorithm();

	Test_PrintCutRodSolution();

	return 0;
}
