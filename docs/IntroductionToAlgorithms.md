# Introduction to Algorithms

## IV Advanced Design and Analysis Techniques
### 15 Dynamic Programming
### 16 Greedy Algorithms
### 17 Amortized Analysis
## V Advanced Data Structures
### 18 B-Trees
### 19 Fibonacci Heap
### 20 Van Emde Boas Trees
### 21 Data Structures for Disjoint Sets
### VI Graph Algorithms
### 22 Elementary Graph Algorithms
### 23 Minimum Spanning Trees
### 24 Single-Source Shortest Paths
### 25 All-Pairs Shortest Paths
### 26 Maximum Flow
## VII Selected Topics
### 27 Multithreaded Algorithms
### 28 Matrix Operations
### 29 Linear Programming
### 30 Polynomials and the FFT
### 31 Number-Theoretic Algorithms
### 32 String Matching
### 33 Computational Geometry
### 34 NP-Completeness
### 35 Approximation Algorithms
## VIII Appendix: Mathematical Background
### A Summations
### B Sets, Etc.
### C Counting and Probability
### D Matrices
