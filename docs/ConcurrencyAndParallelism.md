# Concurrency and Parallelism

## Parallel Computing Hardware

### Sequential versus parallel computing

> Formula One Pit Lane Example

#### Serial Execution

|    Mechanic One    |
|:------------------:|
| Raise The Car      |
| Change Front Left  |
| Change Front Right |
| Change Rear Left   |
| Change Rear Right  |
| Lower The Car      |

#### Parallel Execution

|    Mechanic One    |    Mechanic Two    |
|:------------------:|:------------------:|
|  Raise The Car     | Raise The Car      |
| Change Front Left  | Change Front Right |
| Change Rear Left   | Change Rear Right  |
| Lower The Car      | Lower The Car      |


### Parallell computing architectures

#### **Flynn's Taxonomy**

|                   | **Single Instruction** | **Multiple Instruction** |
| ----------------- |          :--:          |           :--:           |
| **Single Data**   |          SISD          |           MISD           |
| **Multiple Data** |          SIMD          |           MIMD           |

#### **Parallel Programming Model** (MIMD)

* Single Program, Multiple Data (SPMD)
* Multiple Program, Multiple Data (MPMD)

### Shared versus distributed memory

#### **Shared Memory**

* All processors access the same memory with global address space

#### **Shared Memory Architecture**

* Uniform memory access (UMA)
* * Symmetric Multiprocessing (SMP)
* Non-uniform memory access (NUMA)
* * multiple SMP systems together

#### **Distributed Memory**

* Local Memory with local address space, connected to a **network**

#### **Distributed Memory Architecture**

* Supercomputers use distributed memory

## Threads and Processes

### Threads versus process

#### **Processes**

* Includes code, data, and state information
* Independent instance of a running program
* Separate address space

#### **Threads**

* Independent path of execution
* Subset of a process
* Operating system schedules threads of execution

#### **Inter-Process Communication (IPC)**

* Sockets and pipes
* Shared memory
* Remote procedure calls

### Concurrent versus parallel execution

### Execution scheduling

### Thread life cycle


## Designing Parallel Programs

### Partitioning

* Break the problem down into discrete pieces of work
* * Domain/Data Decomposition
* * * Block Decomposition
* * * Cyclic Decomposition
* * Functional Decomposition
* * * Tasks (Process > Update > Render)

### Communication

> * Coordinate task execution and share information
> * * Point-to-Point Communication (Sender > Receiver)
> * * Collective Communication (Broadcast / Scatter) > (Gather)

####

> * Synchronous Blocking Communication
> * * Tasks wait until entire communication is complete
> * * Cannot do other work while in progress
> * Asynchronous Nonblocking Communication
> * * Tasks do not wait for communication to complete
> * * Can do other work while in progress

####

> * **Overhead** - Compute time/resources spent on communication
> * **Latency** - Time to send message from A to B (microseconds)
> * **Bandwidth** - Amount of data communicated per seconds (GB/s)

### Agglomeration

> * Combine tasks and replicate data/computation to increase efficiency
> * **Granularity** = computation/communication

#### **Fine-Grained Parallelism**

* Large number of small tasks
* Advantage - Good distribution of workload (load balancing)
* Disadvantage - Low computation-to-communication ratio

#### **Coarse-Grained Parallelism**

* Small number of large tasks
* Advantage - High computation-to-computation ratio
* Disadvantage - Inefficient load balancing

### Mapping

> * Specify where each task will execute

> **Does not apply to...**
> * single core processors
> * automated task scheduled

#### **Goal**

> * Minimize the total execution time

#### **Strategies**

> * Place tasks that can execute concurrently on different processors
> * Place tasks that communicate frequently on the same processor

## See Also

https://en.cppreference.com/w/cpp/thread/hardware_destructive_interference_size
