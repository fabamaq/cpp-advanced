# C++ Advanced KS

A C++ Advanced Knowledge Sharing experience.

---
## DISCLAIMER
> This project is under constructions.
---

## Download

Download the zip archive and extract the files or clone the project:

```bash
  git clone --recurse-submodules git@gitlab.pt.fabamaq.com:knowledge-sharings/cppseries/internal/cpp-advanced.git
```

### Build Requirements

* [g++](https://gcc.gnu.org/) (10.3 or higher)
* [cmake](https://cmake.org/) (3.20.0 or higher)
* * [make](https://www.gnu.org/software/make/) (4.2.1 or higher)

### Build Process

With `cmake`

```bash
# From the root directory

# Generate the build system
# run cmake . -B <build_directory> -D CMAKE_BUILD_TYPE=[Debug|Release]
# Example
cmake . -B build/Debug/ -D CMAKE_BUILD_TYPE=Debug

# Build the project
# run cmake --build <build_directory> --target <TARGET>
# Example
cmake --build/Debug --target main17
```

Or you can setup your Integrated Development Environment (IDE) with cmake but, instructions for that, are outside the scope of this README (which can be updated with images if you want).

## Main Projects

If using `cmake`, all projects include a pre_compiled_headers library of the C++ Standard Library and a project_global_settings Interface Target to be linked to individual projects. 

### Main17

A recap of c++11 and c++14 code with standard library containers

## Examples Projects

The examples folder contains code about Templates, as well as C++17 sample code. It also includes some examples of Containers and Algorithms (i.e. Dynamic Programming) - but these are incomplete.

### **C++ Templates**

* C++ Templates is a book by by David Vandevoorde, Nicolai M. Josuttis, and Douglas Gregor, giving guidance on how to use Templates in Modern C++.
* You can buy the book at [Amazon](http://www.tmplbook.com/).
* You can find the code [here](http://www.tmplbook.com/).

### **C++17 The Complete Guide**

* C++17 The Complete Guide is a book from Nicolai M. Josuttis that describes how to use the ne language and library features of C++17.
* You can find the draft of the book at  [here](http://www.cppstd17.com/).
* You can find the code [here](http://www.cppstd17.com/).

## External Projects

At the moment, the external folder contains the Google Test Framework only, which is added to cmake automatically.

## Authors

- [Pedro André Oliveira](https://www.linkedin.com/in/pedroandreoliveira/)

## Contributing

Contributions are always welcome!

## Related Knowledge Sharings

* [C++ Test-Driven Development](git@gitlab.pt.fabamaq.com:knowledge-sharings/cppseries/internal/cpp-test-driven-development.git)
* [C++ Design Patterns KS](git@gitlab.pt.fabamaq.com:knowledge-sharings/cppseries/internal/cpp-design-patterns.git)
* [C++ Clean Architecture KS](git@gitlab.pt.fabamaq.com:knowledge-sharings/cppseries/internal/cpp-clean-architecture.git)

* [C++ Fundamentals KS](git@gitlab.pt.fabamaq.com:knowledge-sharings/cppseries/internal/cpp-fundamentals.git)
* [C++ Intermediate KS](git@gitlab.pt.fabamaq.com:knowledge-sharings/cppseries/internal/cpp-intermediate.git)
* [C++ Advanced KS](git@gitlab.pt.fabamaq.com:knowledge-sharings/cppseries/internal/cpp-advanced.git)
* [C++ Mastery KS](git@gitlab.pt.fabamaq.com:knowledge-sharings/cppseries/internal/cpp-mastery.git)

## References

* [Introduction to Algorithms](https://www.amazon.com/Introduction-Algorithms-fourth-Thomas-Cormen/dp/026204630X/)

### YouTube

* [Dynamic Programming - Learn to Solve Algorithmic Problems & Coding Challenges](https://www.youtube.com/watch?v=oBt53YbR9Kk)
* [15. Dynamic Programming, Part 1: SRTBOT, Fib, DAGs, Bowling](https://www.youtube.com/watch?v=r4-cftqTcdI)
